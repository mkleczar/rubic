package pub.mk.rubic.cube2x2x2.cube.model;

import org.junit.Test;
import pub.mk.rubic.cube2x2x2.cube.model.cube.CubeByPermutation;
import pub.mk.rubic.cube2x2x2.cube.model.show.ShowCubeAsNet;

public class CubeDisplayTest {

    @Test
    public void showInit() {
        Cube cube = CubeByPermutation.init();
        ShowCube show = new ShowCubeAsNet();
        String toShow = show.display(cube);
        System.out.println(toShow);
    }

}
