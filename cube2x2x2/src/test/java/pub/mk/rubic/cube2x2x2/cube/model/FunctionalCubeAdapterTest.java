package pub.mk.rubic.cube2x2x2.cube.model;

import org.junit.Test;
import pub.mk.rubic.cube2x2x2.cube.model.cube.CubeByPermutation;
import pub.mk.rubic.cube2x2x2.cube.model.show.ShowCubeAsNet;

import java.util.function.Function;
import static pub.mk.rubic.cube2x2x2.cube.model.FunctionalCubeAdapter.*;

public class FunctionalCubeAdapterTest {

    @Test
    public void testFunctionalCubeAdapter() {
        Function<Cube, Cube> snake = right
                .andThen(right)
                .andThen(down)
                .andThen(down)
                .andThen(right)
                .andThen(right);
        Cube c = snake.apply(CubeByPermutation.init());
        ShowCube show = new ShowCubeAsNet();
        System.out.println(show.display(c));
    }
}
