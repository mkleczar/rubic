package pub.mk.rubic.cube2x2x2.cube.utils;

import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

public class PermutationBuilderTest {

    @Test
    public void identityPermutationTest() {
        int [] result = PermutationBuilder.builder(8).get();
        assertArrayEquals(new int [] {0,1,2,3,4,5,6,7}, result);
    }

    @Test
    public void cycleTest() {
        int [] result = PermutationBuilder.builder(8)
                .cycle(2,3,5)
                .get();
        assertArrayEquals(new int [] {0,1,5,2,4,3,6,7}, result);
    }

    @Test
    public void cycleTwiceTest() {
        int [] result = PermutationBuilder.builder(8)
                .cycle(0,1)
                .cycle(3,4,5)
                .get();
        assertArrayEquals(new int [] {1,0,2,5,3,4,6,7}, result);
    }

    @Test
    public void cycleGroupTest() {
        int [] result = PermutationBuilder.builder(8)
                .cycleGroup(new int [] {0,1}, new int [] {3,4}, new int[] {6,7})
                .get();
        assertArrayEquals(new int [] {6,7,2,0,1,5,3,4}, result);
    }
}
