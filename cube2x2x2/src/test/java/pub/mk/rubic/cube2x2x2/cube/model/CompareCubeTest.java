package pub.mk.rubic.cube2x2x2.cube.model;

import org.junit.Test;
import pub.mk.rubic.cube2x2x2.cube.model.show.ShowCubeAsNet;
import pub.mk.rubic.cube2x2x2.cube.utils.ColourPermutation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CompareCubeTest {

    @Test
    public void testCompareWithInitialCube() {
        Cube cube = CubeFactory.create();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("************************")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWW********************")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYY****************")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYYGGGG************")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYYGGGGBBBB********")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYYGGGGBBBBRRRR****")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYYGGGGBBBBRRRROOOO")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWW*********B******OOOO")));

        assertFalse(CompareCube.isLike(cube, ColourPermutation.compareBuilder("YYYY********************")));
        assertFalse(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYY****O***********")));
        assertFalse(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYYWWWW************")));
        assertFalse(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYYGGGGBBBBOO******")));
        assertFalse(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYYGGGGBBBBRRRRR***")));
        assertFalse(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWWYYYYGGGGBBBBRRROOOOO")));
        assertFalse(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWW****Y***********OOOO")));
    }

    @Test
    public void testCompareWithLeftMoveCube() {
        Cube cube = CubeFactory.create().left();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("BBWW **** **** **** **** ****")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("BBWW GGYY **** **** **** ****")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("BBWW GGYY WWGG **** **** ****")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("BBWW GGYY WWGG BBYY **** ****")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("**** **** **** **** RRRR OOOO")));
    }

    @Test
    public void testCompareWithUpMoveCube() {
        Cube cube = CubeFactory.create().up();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWW YYYY****************")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("**** **** RGRG **** **** ****")));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("**** **** **** OBOB BRBR GOGO")));
    }

    @Test
    public void testCompareWithFrontMoveCube() {
        Cube cube = CubeFactory.create();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWW YYYY GGGG BBBB RRRR OOOO")));
        cube = cube.front();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WOWO RYRY GGGG BBBB WWRR OOYY")));
    }

    @Test
    public void testCompareWitRightMoveCube() {
        Cube cube = CubeFactory.create().right();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWGG YYBB GGYY WWBB RRRR OOOO")));
    }

    @Test
    public void testCompareWitDownMoveCube() {
        Cube cube = CubeFactory.create().down();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWW YYYY GOGO BRBR RGRG OBOB")));
    }

    @Test
    public void testCompareWitBackMoveCube() {
        Cube cube = CubeFactory.create().back();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("RWRW YOYO GGGG BBBB RRYY WWOO")));
    }

    @Test
    public void testCompareWithLeftAcMoveCube() {
        Cube cube = CubeFactory.create().leftAc();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("GGWW BBYY YYGG BBWW RRRR OOOO")));
    }

    @Test
    public void testCompareWithUpAcMoveCube() {
        Cube cube = CubeFactory.create().upAc();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWW YYYY OGOG RBRB GRGR BOBO")));
    }

    @Test
    public void testCompareWithFrontAcMoveCube() {
        Cube cube = CubeFactory.create().frontAc();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WRWR OYOY GGGG BBBB YYRR OOWW")));
    }

    @Test
    public void testCompareWitRightAcMoveCube() {
        Cube cube = CubeFactory.create().rightAc();
        //ShowCube show = new ShowCubeAsNet();
        //System.out.println(show.display(cube));
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWBB YYGG GGWW YYBB RRRR OOOO")));
    }
    @Test
    public void testCompareWitDownAcMoveCube() {
        Cube cube = CubeFactory.create().downAc();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("WWWW YYYY GRGR BOBO RBRB OGOG")));
    }
    @Test
    public void testCompareWitBackAcMoveCube() {
        Cube cube = CubeFactory.create().backAc();
        assertTrue(CompareCube.isLike(cube, ColourPermutation.compareBuilder("OWOW YRYR GGGG BBBB RRWW YYOO")));
    }
}
