package pub.mk.rubic.cube2x2x2.cube.model;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import pub.mk.rubic.cube2x2x2.cube.model.cube.CubeByPermutation;
import pub.mk.rubic.cube2x2x2.cube.model.show.ShowCubeAsNet;
import pub.mk.rubic.cube2x2x2.cube.utils.ColourPermutation;
import pub.mk.rubic.cube2x2x2.cube.utils.Streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static org.junit.Assert.*;
import static pub.mk.rubic.cube2x2x2.cube.model.Colour.*;
import static pub.mk.rubic.cube2x2x2.cube.model.FunctionalCubeAdapter.*;

public class CubeByPermutationTest {

    @Test
    public void testRotationFrontAc() {
        Cube c = CubeByPermutation.init()
                .frontAc()
                ;
        Colour [][][] expected = ColourPermutation.builder(
                White, Brown, White, Brown,
                Orange, Yellow, Orange, Yellow,
                Green, Green, Green, Green,
                Blue, Blue, Blue, Blue,
                Yellow, Yellow, Brown, Brown,
                Orange, Orange, White, White);
        assertArrayEquals(expected, c.show());
        //ShowCube show = new ShowCubeAsNet();
        //System.out.println(show.display(c));
    }
    @Test
    public void testRotationRightAc() {
        Cube c = CubeByPermutation.init()
                .rightAc()
                ;
        Colour [][][] expected = ColourPermutation.builder(
                White, White, Blue, Blue,
                Yellow, Yellow, Green, Green,
                Green, Green, White, White,
                Yellow, Yellow, Blue, Blue,
                Brown, Brown, Brown, Brown,
                Orange, Orange, Orange,Orange);
        //ShowCube show = new ShowCubeAsNet();
        //System.out.println(show.display(c));
        assertArrayEquals(expected, c.show());
    }

    @Test
    public void testSimpleShape() {
        Cube c = CubeByPermutation.init()
                .right()
                .right()
                .down()
                .down()
                .right()
                .right();
        Colour[][][] expected = ColourPermutation.builder(
                White, White, Yellow, Yellow,
                White, White, Yellow, Yellow,
                Green, Green, Green, Green,
                Blue, Blue, Blue, Blue,
                Orange, Brown, Orange, Brown,
                Orange, Brown, Orange, Brown
        );
        assertArrayEquals(expected, c.show());
        //ShowCube show = new ShowCubeAsNet();
        //System.out.println(show.display(c));
    }

    @Test
    public void testEqualsTwoClockwiseAndTwoAnticlockwiseRotationForEachSite() {
        List<Function<Cube, Cube>> rotation = Arrays.asList(
                up.andThen(up), down.andThen(down), left.andThen(left), right.andThen(right), front.andThen(front), back.andThen(back)
        );
        List<Function<Cube, Cube>> rotationAc = Arrays.asList(
                upAc.andThen(upAc), downAc.andThen(downAc), leftAc.andThen(leftAc), rightAc.andThen(rightAc), frontAc.andThen(frontAc), backAc.andThen(backAc)
        );

        final Cube c = CubeByPermutation.init();

        // two clockwise is the same as two anticlockwise
        Streams.zip(rotation.stream(), rotationAc.stream(), Pair::of)
                .map(p -> Pair.of(p.getLeft().apply(c), p.getRight().apply(c)))
                .forEach(p -> assertArrayEquals(p.getLeft().show(), p.getRight().show()));
    }

    @Test
    public void testEqualsOneClockwiseAndThreeAnticlockwiseRotationForEachSite() {
        List<Function<Cube, Cube>> rotation = Arrays.asList(
                up, down, left, right, front, back
        );
        List<Function<Cube, Cube>> rotationAc = Arrays.asList(
                upAc.andThen(upAc).andThen(upAc),
                downAc.andThen(downAc).andThen(downAc),
                leftAc.andThen(leftAc).andThen(leftAc),
                rightAc.andThen(rightAc).andThen(rightAc),
                frontAc.andThen(frontAc).andThen(frontAc),
                backAc.andThen(backAc).andThen(backAc)
        );

        final Cube c = CubeByPermutation.init();

        // one clockwise is the same as three anticlockwise
        Streams.zip(rotation.stream(), rotationAc.stream(), Pair::of)
                .map(p -> Pair.of(p.getLeft().apply(c), p.getRight().apply(c)))
                .forEach(p -> assertArrayEquals(p.getLeft().show(), p.getRight().show()));
    }

    @Test
    public void testEqualsThreeClockwiseAndOneAnticlockwiseRotationForEachSite() {
        List<Function<Cube, Cube>> rotation = Arrays.asList(
                up.andThen(up).andThen(up),
                down.andThen(down).andThen(down),
                left.andThen(left).andThen(left),
                right.andThen(right).andThen(right),
                front.andThen(front).andThen(front),
                back.andThen(back).andThen(back)
        );
        List<Function<Cube, Cube>> rotationAc = Arrays.asList(
                upAc, downAc, leftAc, rightAc, frontAc, backAc
        );

        final Cube c = CubeByPermutation.init();

        // three clockwise is the same as one anticlockwise
        Streams.zip(rotation.stream(), rotationAc.stream(), Pair::of)
                .map(p -> Pair.of(p.getLeft().apply(c), p.getRight().apply(c)))
                .forEach(p -> assertArrayEquals(p.getLeft().show(), p.getRight().show()));
    }
}
