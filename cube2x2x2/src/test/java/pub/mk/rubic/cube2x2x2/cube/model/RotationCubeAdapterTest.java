package pub.mk.rubic.cube2x2x2.cube.model;

import org.junit.Test;
import pub.mk.rubic.cube2x2x2.cube.model.show.ShowCubeAsNet;

import static org.junit.Assert.assertEquals;

public class RotationCubeAdapterTest {

    @Test
    public void rotation() {
        RotationCubeAdapter c = RotationCubeAdapter.create()
                .rotate("RRDDRR");

        ShowCube show = new ShowCubeAsNet();
        System.out.println(show.display(c.getCurrentCube()));
    }

    @Test
    public void testReverseOneRotationLongSequence() {
        assertEquals("F", RotationCubeAdapter.reverseSequence("f"));
        assertEquals("f", RotationCubeAdapter.reverseSequence("F"));
        assertEquals("B", RotationCubeAdapter.reverseSequence("b"));
        assertEquals("b", RotationCubeAdapter.reverseSequence("B"));
        assertEquals("U", RotationCubeAdapter.reverseSequence("u"));
        assertEquals("u", RotationCubeAdapter.reverseSequence("U"));
        assertEquals("D", RotationCubeAdapter.reverseSequence("d"));
        assertEquals("d", RotationCubeAdapter.reverseSequence("D"));
        assertEquals("R", RotationCubeAdapter.reverseSequence("r"));
        assertEquals("r", RotationCubeAdapter.reverseSequence("R"));
        assertEquals("L", RotationCubeAdapter.reverseSequence("l"));
        assertEquals("l", RotationCubeAdapter.reverseSequence("L"));
    }

    @Test
    public void testReverseTwoRotationsLongSequence() {
        assertEquals("fF", RotationCubeAdapter.reverseSequence("fF"));
        assertEquals("fBuDrL", RotationCubeAdapter.reverseSequence("lRdUbF"));
        assertEquals("", RotationCubeAdapter.reverseSequence(""));
    }
}
