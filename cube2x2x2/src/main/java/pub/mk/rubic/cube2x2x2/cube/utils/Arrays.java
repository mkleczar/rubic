package pub.mk.rubic.cube2x2x2.cube.utils;

public class Arrays {

    @SafeVarargs
    public static <T> void fill(T[][][] destination, T ... items) {
        for (int i = 0; i < 6; ++i) {
            destination[i][0][0] = items[4*i];
            destination[i][0][1] = items[4*i + 1];
            destination[i][1][0] = items[4*i + 2];
            destination[i][1][1] = items[4*i + 3];
        }
    }

    public static <T> void flat(T[] destination, T[][][] source) {
        for (int i = 0; i < source.length; ++i) {
            for (int j = 0; j < source[0].length; ++j) {
                for (int k = 0; k < source[0][0].length; ++k) {
                    int index = k + source[i][j].length * (j + source[i].length * i);
                    destination[index] = source[i][j][k];
                }
            }
        }
    }

}
