package pub.mk.rubic.cube2x2x2.cube.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum Colour {
    White("W"),
    Yellow("Y"),
    Green("G"),
    Blue("B"),
    Orange("O"),
    Brown("R");

    private static final Map<String, Colour> shortcutMap = new HashMap<>();

    static {
        Arrays.stream(Colour.values())
                .forEach(c -> shortcutMap.put(c.shortcut, c));
    }

    private String shortcut;

    Colour(String shortcut) {
        this.shortcut = shortcut;
    }

    public String getShortcut() {
        return shortcut;
    }

    public static Colour forShortcut(String shortcut) {
        return shortcutMap.get(shortcut);
    }
}
