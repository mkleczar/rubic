package pub.mk.rubic.cube2x2x2.cube.model;

public interface ShowCube {
    String display(Cube cube);
}
