package pub.mk.rubic.cube2x2x2.cube.model.cube;

import org.apache.commons.lang3.builder.EqualsBuilder;
import pub.mk.rubic.cube2x2x2.cube.model.Colour;
import pub.mk.rubic.cube2x2x2.cube.model.Cube;
import pub.mk.rubic.cube2x2x2.cube.utils.ColourPermutation;
import pub.mk.rubic.cube2x2x2.cube.utils.PermutationBuilder;

public class CubeByPermutation implements Cube {

    // order of sites: up W, front G, left O, right B, back L, down Y
    //       3W 0W
    //       2W 1W
    // 3O 0O 3G 0G 3B 0B 3L 0L
    // 2O 1O 2G 1G 2B 1B 2L 1L
    //       3Y 0Y
    //       2Y 1Y
    //
    //          [ 3][ 0]
    //          [ 2][ 1]
    // [11][ 8] [ 7][ 4] [15][12] [19][16]
    // [10][ 9] [ 6][ 5] [14][13] [18][17]
    //          [23][20]
    //          [22][21]
    //
    private static final Colour [] initial = {
            Colour.White, Colour.White, Colour.White, Colour.White,
            Colour.Green, Colour.Green, Colour.Green, Colour.Green,
            Colour.Orange, Colour.Orange, Colour.Orange, Colour.Orange,
            Colour.Brown, Colour.Brown, Colour.Brown, Colour.Brown,
            Colour.Blue, Colour.Blue, Colour.Blue, Colour.Blue,
            Colour.Yellow, Colour.Yellow, Colour.Yellow, Colour.Yellow
    };

    private static final int cubeArraySize = 24;

    private static final int [] showPermutation = {3,2,0,1,23,22,20,21,7,6,4,5,19,18,16,17,15,14,12,13,11,10,8,9};

    private static int [] up = PermutationBuilder.builder(cubeArraySize)
            .cycle(3,2,1,0)
            .cycleGroup(array(19,16), array(15,12), array(7,4), array(11,8))
            .get();
    private static int [] upAc = PermutationBuilder.builder(cubeArraySize)
            .cycle(0,1,2,3)
            .cycleGroup(array(11,8), array(7,4), array(15,12), array(19,16))
            .get();
    private static int [] left = PermutationBuilder.builder(cubeArraySize)
            .cycle(8,9,10,11)
            .cycleGroup(array(3,2), array(7,6), array(23,22), array(17,16))
            .get();
    private static int [] leftAc =PermutationBuilder.builder(cubeArraySize)
            .cycle(11,10,9,8)
            .cycleGroup(array(23,22), array(7,6), array(3,2), array(17,16))
            .get();
    private static int [] down = PermutationBuilder.builder(cubeArraySize)
            .cycle(20,21,22,23)
            .cycleGroup(array(6,5), array(14,13), array(18,17), array(10,9))
            .get();
    private static int [] downAC = PermutationBuilder.builder(cubeArraySize)
            .cycle(23,22,21,20)
            .cycleGroup(array(18,17), array(14,13), array(6,5), array(10,9))
            .get();
    private static int [] front = PermutationBuilder.builder(cubeArraySize)
            .cycle(4,5,6,7)
            .cycleGroup(array(2,1), array(15,14), array(20,23), array(9,8))
            .get();
    private static int [] frontAC = PermutationBuilder.builder(cubeArraySize)
            .cycle(7,6,5,4)
            .cycleGroup(array(2,1), array(9,8), array(20,23), array(15,14))
            .get();
    private static int [] right = PermutationBuilder.builder(cubeArraySize)
            .cycle(12,13,14,15)
            .cycleGroup(array(20,21), array(4,5), array(0,1), array(18,19))
            .get();
    private static int [] rightAC = PermutationBuilder.builder(cubeArraySize)
            .cycle(15,14,13,12)
            .cycleGroup(array(0,1), array(4,5), array(20,21), array(18,19))
            .get();
    private static int [] back =  PermutationBuilder.builder(cubeArraySize)
            .cycle(16,17,18,19)
            .cycleGroup(array(12,13), array(3,0), array(10,11), array(21,22))
            .get();
    private static int [] backAC =  PermutationBuilder.builder(cubeArraySize)
            .cycle(19,18,17,16)
            .cycleGroup(array(12,13), array(21,22), array(10,11), array(3,0))
            .get();

    private Colour [] cube = new Colour[cubeArraySize];

    public static Cube init() {
        return new CubeByPermutation(initial);
    }

    private CubeByPermutation(Colour [] colours) {
        assert colours.length == cubeArraySize;
        cube = new Colour[colours.length];
        System.arraycopy(colours, 0, cube, 0, colours.length);
    }

    @Override
    public Cube front() {
        return byPermutation(cube, front);
    }

    @Override
    public Cube frontAc() {
        return byPermutation(cube, frontAC);
    }

    @Override
    public Cube back() {
        return byPermutation(cube, back);
    }

    @Override
    public Cube backAc() {
        return byPermutation(cube, backAC);
    }

    @Override
    public Cube up() {
        return byPermutation(cube, up);
    }

    @Override
    public Cube upAc() {
        return byPermutation(cube, upAc);
    }

    @Override
    public Cube down() {
        return byPermutation(cube, down);
    }

    @Override
    public Cube downAc() {
        return byPermutation(cube, downAC);
    }

    @Override
    public Cube left() {
        return byPermutation(cube, left);
    }

    @Override
    public Cube leftAc() {
        return byPermutation(cube, leftAc);
    }

    @Override
    public Cube right() {
        return byPermutation(cube, right);
    }

    @Override
    public Cube rightAc() {
        return byPermutation(cube, rightAC);
    }

    @Override
    public Colour[][][] show() {
        // transform colour array according order required by interface description
        Colour [] toShow = ColourPermutation.permutation(cube, showPermutation);
        Colour [][][] result = new Colour[6][2][2];
        for (int c = 0; c < 6; ++c) {
            for (int i = 0; i < 2; ++i) {
                for (int j = 0; j < 2; ++j) {
                    result[c][i][j] = toShow[c*4 + i*2 + j ];
                }
            }
        }
        return result;
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CubeByPermutation that = (CubeByPermutation) o;

        return new EqualsBuilder()
                .append(cube, that.cube)
                .isEquals();
    }

    @Override public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(cube)
                .toHashCode();
    }

    private static Cube byPermutation(Colour [] colours, int [] permutation) {
        return new CubeByPermutation(ColourPermutation.permutation(colours, permutation));
    }

    private static int[] array(int ... a) {
        return a;
    }
}
