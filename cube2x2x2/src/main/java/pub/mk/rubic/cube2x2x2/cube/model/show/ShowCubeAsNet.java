package pub.mk.rubic.cube2x2x2.cube.model.show;

import pub.mk.rubic.cube2x2x2.cube.model.Colour;
import pub.mk.rubic.cube2x2x2.cube.model.Cube;
import pub.mk.rubic.cube2x2x2.cube.model.ShowCube;

public class ShowCubeAsNet implements ShowCube {

    @Override
    public String display(Cube c) {
        String align = "   ";
        Colour[][][] show = c.show();
        StringBuffer l0 = new StringBuffer().append(align);
        StringBuffer l1 = new StringBuffer().append(align);
        StringBuffer l2 = new StringBuffer();
        StringBuffer l3 = new StringBuffer();
        StringBuffer l4 = new StringBuffer().append(align);
        StringBuffer l5 = new StringBuffer().append(align);

        toBuffer(show[0], l0, l1);
        toBuffer(show[1], l4, l5);
        toBuffer(show[5], l2, l3);
        toBuffer(show[2], l2, l3);
        toBuffer(show[4], l2, l3);
        toBuffer(show[3], l2, l3);

        l0.append(System.lineSeparator());
        l1.append(System.lineSeparator());
        l2.append(System.lineSeparator());
        l3.append(System.lineSeparator());
        l4.append(System.lineSeparator());
        l5.append(System.lineSeparator());

        return l0.append(l1).append(l2).append(l3).append(l4).append(l5).toString();
    }

    private static void toBuffer(Colour[][] colours, StringBuffer b1, StringBuffer b2) {
        b1.append(colours[0][0].getShortcut()).append(colours[1][0].getShortcut()).append(" ");
        b2.append(colours[0][1].getShortcut()).append(colours[1][1].getShortcut()).append(" ");
    }

}
