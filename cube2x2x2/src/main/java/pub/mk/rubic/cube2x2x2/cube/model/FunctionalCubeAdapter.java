package pub.mk.rubic.cube2x2x2.cube.model;

import java.util.function.Function;

public class FunctionalCubeAdapter {

    public static Function<Cube, Cube> front = Cube::front;
    public static Function<Cube, Cube> back = Cube::back;
    public static Function<Cube, Cube> up = Cube::up;
    public static Function<Cube, Cube> down = Cube::down;
    public static Function<Cube, Cube> left = Cube::left;
    public static Function<Cube, Cube> right = Cube::right;

    public static Function<Cube, Cube> frontAc = Cube::frontAc;
    public static Function<Cube, Cube> backAc = Cube::backAc;
    public static Function<Cube, Cube> upAc = Cube::upAc;
    public static Function<Cube, Cube> downAc = Cube::downAc;
    public static Function<Cube, Cube> leftAc = Cube::leftAc;
    public static Function<Cube, Cube> rightAc = Cube::rightAc;
}
