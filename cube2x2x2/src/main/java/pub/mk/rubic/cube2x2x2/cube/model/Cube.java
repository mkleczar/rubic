package pub.mk.rubic.cube2x2x2.cube.model;

public interface Cube {

    Cube front();
    Cube frontAc();

    Cube back();
    Cube backAc();

    Cube up();
    Cube upAc();

    Cube down();
    Cube downAc();

    Cube left();
    Cube leftAc();

    Cube right();
    Cube rightAc();

    // six arrays 2x2;
    // order of sites: up - White, down - Yellow, front - Green, back - Blue, right - Brown, left - Orange
    //           up up
    //           up up
    //     le le fr fr ri ri ba ba
    //     le le fr fr ri ri ba ba
    //           do do
    //           do do
    //     [0]
    //  [5][2][4][3]
    //     [1]
    //           [00][10]
    //           [01][11]
    //   [00][10][00][10][00][10][00][10]
    //   [01][11][01][11][01][11][01][11]
    //           [00][10]
    //           [01][11]
    Colour [][][] show();
}
