package pub.mk.rubic.cube2x2x2.cube.utils;

import java.util.Arrays;

public class PermutationBuilder {

    private int [] permutation;

    public static PermutationBuilder builder(int size) {
        return new PermutationBuilder(size);
    }

    private PermutationBuilder(int size) {
        this.permutation = new int [size];
        Arrays.setAll(this.permutation, i -> i);
    }

    // cycle: 2,4,6 rotate 2 to 4, 4 to 6 and 6 to 2,
    // namely: cycle 2,4,6 on permutation 0,1,2,3,4,5,6 gives 0,1,6,3,2,5,4
    public PermutationBuilder cycle(int ... c) {
        int root = c[0];
        for (int i = 1; i < c.length; ++i) {
            int tmp = permutation[root];
            permutation[root] = permutation[c[i]];
            permutation[c[i]] = tmp;
        }
        return this;
    }

    // same as cycle, but rotates whole groups instead of individual values
    // namely: cycle group {0,1}, {3,4} on permutation 0,1,2,3,4,5 gives 3,4,2,0,1,5
    public PermutationBuilder cycleGroup(int [] ... cg) {
        int groupSize = cg[0].length;
        for (int i = 0; i < groupSize; ++i) {
            int [] microCycle = new int [cg.length];
            for (int k = 0; k < cg.length; ++k) {
                microCycle[k] = cg[k][i];
            }
            cycle(microCycle);
        }
        return this;
    }

    public int [] get() {
        return Arrays.copyOf(permutation, permutation.length);
    }
}
