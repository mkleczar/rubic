package pub.mk.rubic.cube2x2x2.cube.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

public class RotationCubeAdapter {

    private static final Map<String, Function<Cube,Cube>> rotationMap = new HashMap<>();
    static {
        rotationMap.put("F", Cube::front);
        rotationMap.put("f", Cube::frontAc);
        rotationMap.put("B", Cube::back);
        rotationMap.put("b", Cube::backAc);
        rotationMap.put("U", Cube::up);
        rotationMap.put("u", Cube::upAc);
        rotationMap.put("D", Cube::down);
        rotationMap.put("d", Cube::downAc);
        rotationMap.put("R", Cube::right);
        rotationMap.put("r", Cube::rightAc);
        rotationMap.put("L", Cube::left);
        rotationMap.put("l", Cube::leftAc);
    }

    private Cube start;
    private Cube current;
    private String rotations = "";

    private RotationCubeAdapter(Cube cube) {
        this.start = cube;
        this.current = cube;
    }

    public static RotationCubeAdapter create() {
        return new RotationCubeAdapter(CubeFactory.create());
    }

    public static RotationCubeAdapter from(Cube cube) {
        return new RotationCubeAdapter(cube);
    }

    public static String reverseSequence(String sequence) {
        return Objects.requireNonNull(sequence)
                .chars()
                .mapToObj(c -> (char)c)
                .map(c -> (c < 'a') ? (""+c).toLowerCase() : (""+c).toUpperCase())
                .reduce("", (s1,s2) -> s2 + s1);
    }

    public RotationCubeAdapter rotate(String multiple) {
        multiple.chars()
                .mapToObj(c -> (char)c)
                .forEach(this::rotate);
        return this;
    }

    public RotationCubeAdapter rotate(char kind) {
        current = rotationMap.get("" + kind).apply(current);
        rotations += kind;
        return this;
    }

    public Cube getStartCube() {
        return this.start;
    }

    public Cube getCurrentCube() {
        return this.current;
    }

    public String getRotations() {
        return this.rotations;
    }
}
