package pub.mk.rubic.cube2x2x2.cube.model;

import static pub.mk.rubic.cube2x2x2.cube.utils.Arrays.flat;

public class CompareCube {

    public static boolean isLike(Cube cube, CompareColour[][][] compareColours) {
        Colour[] colours = new Colour[24];
        CompareColour[] compare = new CompareColour[24];
        flat(colours, cube.show());
        flat(compare, compareColours);

        for (int i = 0; i < compare.length; ++i) {
            if (! (compare[i] == CompareColour.ANY || compare[i].isEqual(colours[i]))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNotLike(Cube cube, CompareColour[][][] compareColours) {
        return !isLike(cube, compareColours);
    }
}
