package pub.mk.rubic.cube2x2x2.cube.model;

import pub.mk.rubic.cube2x2x2.cube.model.cube.CubeByPermutation;

public class CubeFactory {
    public static Cube create() {
        return CubeByPermutation.init();
    }
}
