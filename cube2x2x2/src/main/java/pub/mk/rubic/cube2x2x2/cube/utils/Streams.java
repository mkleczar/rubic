package pub.mk.rubic.cube2x2x2.cube.utils;

import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.BiFunction;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Streams {

    public static <S1, S2, V> Stream<V> zip(
            Stream<? extends S1> s1,
            Stream<? extends S2> s2,
            BiFunction<? super S1, ? super S2, ? extends V> zipper) {
        Spliterator<? extends S1> s1Spl = Objects.requireNonNull(s1).spliterator();
        Spliterator<? extends S2> s2Spl = Objects.requireNonNull(s2).spliterator();

        int characteristics = s1Spl.characteristics() & s2Spl.characteristics() &
                ~(Spliterator.DISTINCT | Spliterator.SORTED);

        long zipSize = ((characteristics & Spliterator.SIZED) != 0) ?
                Math.min(s1Spl.getExactSizeIfKnown(), s2Spl.getExactSizeIfKnown()) :
                -1;

        Iterator<S1> s1Iter = Spliterators.iterator(s1Spl);
        Iterator<S2> s2Iter = Spliterators.iterator(s2Spl);
        Iterator<V> vIter = new Iterator<V>() {
            @Override public boolean hasNext() {
                return s1Iter.hasNext() && s2Iter.hasNext();
            }

            @Override public V next() {
                return zipper.apply(s1Iter.next(), s2Iter.next());
            }
        };
        Spliterator<V> zipped = Spliterators.spliterator(vIter, zipSize, characteristics);
        return StreamSupport.stream(zipped,s1.isParallel() || s2.isParallel());
    }
}
