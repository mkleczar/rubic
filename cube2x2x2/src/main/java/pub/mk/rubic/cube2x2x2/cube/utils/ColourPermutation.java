package pub.mk.rubic.cube2x2x2.cube.utils;

import pub.mk.rubic.cube2x2x2.cube.model.Colour;
import pub.mk.rubic.cube2x2x2.cube.model.CompareColour;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static pub.mk.rubic.cube2x2x2.cube.utils.Arrays.fill;


public class ColourPermutation {

    public static Colour [] permutation(Colour [] array, int [] p) {
        assert array.length == p.length;
        Colour [] result = new Colour[array.length];
        for (int i = 0; i < p.length; ++i) {
            result[i] = array[p[i]];
        }
        return result;
    }

    // warning: for test purpose only, not all possible permutation
    // gives setting achievable from initial cube position!
    // colours order as in Cube interface documentation, each site order is 00, 01, 10, 11
    public static Colour[][][] builder(Colour ... colours) {
        assert colours.length == 24;
        Colour [][][] result = new Colour[6][2][2];
        fill(result, colours);
        return result;
    }

    public static CompareColour[][][] compareBuilder(CompareColour ... colours) {
        assert colours.length == 24;
        CompareColour [][][] result = new CompareColour[6][2][2];
        fill(result, colours);
        return result;
    }

    public static CompareColour[][][] compareBuilder(String  compareColourShortcuts) {
        List<CompareColour> list = compareColourShortcuts
                .replaceAll(" ", "")
                .chars()
                .mapToObj(c -> "" + (char)c)
                .map(CompareColour::forShortcut)
                .collect(Collectors.toList());
        CompareColour[] array = new CompareColour[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            array[i] = list.get(i);
        }
        return compareBuilder(array);
    }

}
