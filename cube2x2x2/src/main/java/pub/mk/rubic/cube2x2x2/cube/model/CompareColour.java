package pub.mk.rubic.cube2x2x2.cube.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public enum CompareColour {
    White("W"),
    Yellow("Y"),
    Green("G"),
    Blue("B"),
    Orange("O"),
    Brown("R"),
    ANY("*");

    private static final Map<String, CompareColour> shortcutMap = new HashMap<>();

    static {
        Arrays.stream(CompareColour.values())
                .forEach(c -> shortcutMap.put(c.shortcut, c));
    }

    private String shortcut;

    CompareColour(String shortcut) {
        this.shortcut = shortcut;
    }

    public boolean isEqual(Colour colour) {
        return colour.getShortcut().equals(this.shortcut);
    }

    public static CompareColour forShortcut(String shortcut) {
        return shortcutMap.get(shortcut);
    }
}
